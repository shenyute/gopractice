package main // import "github.com/shenyute/http_upload_server"

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
)

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}
	f := path.Base(r.URL.Path)
	if f == "" || f == "." {
		http.Error(w, "Error", 400)
		return
	}
	fileName := "./uploads/" + f
	file, err := os.Create(fileName)
	if err != nil {
		http.Error(w, "Error", 400)
		return
	}
	n, err := io.Copy(file, r.Body)
	if err != nil {
		http.Error(w, "Error", 400)
		return
	}
	// limit upload file to 100MB
	if n > 100 * 1024 * 1024 {
	    os.Remove(fileName)
		http.Error(w, "Error", 400)
		return
	}

	w.Write([]byte(fmt.Sprintf("%d bytes are recieved.\n", n)))
}

func main() {
	http.HandleFunc("/upload/", uploadHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
